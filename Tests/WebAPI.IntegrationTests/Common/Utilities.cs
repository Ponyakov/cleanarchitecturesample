using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;
using Infrastructure.Persistence;
using Newtonsoft.Json;

namespace WebAPI.IntegrationTests.Common
{
 public class Utilities
    {
        public static StringContent GetRequestContent(object obj)
        {
            return new StringContent(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json");
        }

        public static async Task<T> GetResponseContent<T>(HttpResponseMessage response)
        {
            var stringResponse = await response.Content.ReadAsStringAsync();

            var result = JsonConvert.DeserializeObject<T>(stringResponse);

            return result;
        }

        public static void InitializeDbForTests(ApplicationDbContext context)
        {

            var student = new Student
            {
                 FirstName = "Ivan",
                 Gender = Gender.Male,
                 LastName = "Ivanov",
                 NickName = "cucumber",
                 PatronymicName = "Eliseevich"
            };

            context.Students.Add(student);

            student = new Student
            {
                 FirstName = "Vlad",
                 Gender = Gender.Male,
                 LastName = "Ivanov",
                 NickName = "pepper",
                 PatronymicName = "Eliseevich"
            };

            context.Students.Add(student);

            var group = new Group
            {
                Name = "The Best"                
            };

            context.Groups.Add(group);

            var group2 = new Group
            {
                Name = "Another "                
            };

            context.Groups.Add(group2);


            var groupStudents = new GroupStudent
            {
                Student = student,
                Group = group
            };

            context.GroupStudents.Add(groupStudents);

            groupStudents = new GroupStudent
            {
                Student = student,
                Group = group2
            };

            context.GroupStudents.Add(groupStudents);

            context.SaveChanges();
        }
    }
}
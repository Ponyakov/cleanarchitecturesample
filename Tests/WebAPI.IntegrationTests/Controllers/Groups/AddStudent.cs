using System.Net;
using System.Threading.Tasks;
using Application.Groups.Commands.AddStudentToGroup;
using WebAPI.IntegrationTests.Common;
using Xunit;

namespace WebAPI.IntegrationTests.Controllers.Groups
{
    public class AddStudent : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> _factory;

        public AddStudent(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task GivenAddStudentToGroupCommand_ReturnsSuccessStatusCode()
        {
            var client = _factory.CreateClient();

            var command = new AddStudentToGroupCommand
            {
                StudentId = 1,
                GroupId = 1
            };

            var content = Utilities.GetRequestContent(command);

            var response = await client.PostAsync($"/api/groups/student", content);

            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GivenAddStudentCommandToTheSameGroupTwice_ReturnsSuccessStatusCode()
        {
            var client = _factory.CreateClient();

            var command = new AddStudentToGroupCommand
            {
                StudentId = 1,
                GroupId = 1
            };

            var content = Utilities.GetRequestContent(command);

            var response = await client.PostAsync($"/api/groups/student", content);

            response.EnsureSuccessStatusCode();

            response = await client.PostAsync($"/api/groups/student", content);

            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GivenAddStudentToGroupCommandWithInvalidStudentId_ReturnsNotFoundStatusCode()
        {
            var client = _factory.CreateClient();

            var command = new AddStudentToGroupCommand
            {
                StudentId = 0,
                GroupId = 1
            };

            var content = Utilities.GetRequestContent(command);

            var response = await client.PostAsync($"/api/groups/student", content);

            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

                [Fact]
        public async Task GivenAddStudentToGroupCommandWithInvalidGroupId_ReturnsNotFoundStatusCode()
        {
            var client = _factory.CreateClient();

            var command = new AddStudentToGroupCommand
            {
                StudentId = 1,
                GroupId = 0
            };

            var content = Utilities.GetRequestContent(command);

            var response = await client.PostAsync($"/api/groups/student", content);

            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }
    }
}
using System.Net;
using System.Threading.Tasks;
using WebAPI.IntegrationTests.Common;
using Xunit;

namespace WebAPI.IntegrationTests.Controllers.Groups
{
    public class Delete : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> _factory;

        public Delete(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task GivenId_ReturnsSuccessStatusCode()
        {
            var client = _factory.CreateClient();

            var validId = "1";

            var response = await client.DeleteAsync($"/api/groups/{validId}");

            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GivenInvalidId_ReturnsNotFoundStatusCode()
        {
            var client = _factory.CreateClient();

            var invalidId = "0";

            var response = await client.DeleteAsync($"/api/groups/{invalidId}");

            response.EnsureSuccessStatusCode();
        }
    }
}
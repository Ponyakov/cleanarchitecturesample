using System.Threading.Tasks;
using Application.Groups.Commands.CreateGroup;
using Microsoft.AspNetCore.Mvc.Testing;
using WebAPI.IntegrationTests.Common;
using Xunit;

namespace WebAPI.IntegrationTests.Controllers.Groups
{
    public class Create : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> _factory;

        public Create(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task GivenCreateGroupCommand_ReturnsSuccessStatusCode()
        {
            var client = await _factory.GetAuthenticatedClientAsync();

            var command = new CreateGroupCommand
            {
                Name = "Group 1"                
            };

            var content = Utilities.GetRequestContent(command);

            var response = await client.PostAsync($"/api/groups", content);

            var id = await Utilities.GetResponseContent<int>(response);

            Assert.True(id > 0);

            response.EnsureSuccessStatusCode();
        }
    }
}
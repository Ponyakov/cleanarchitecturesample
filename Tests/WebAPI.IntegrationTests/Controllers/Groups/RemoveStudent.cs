using System.Threading.Tasks;
using Application.Groups.Commands.RemoveStudentFromGroup;
using WebAPI.IntegrationTests.Common;
using Xunit;

namespace WebAPI.IntegrationTests.Controllers.Students
{
    public class RemoveStudent : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> _factory;

        public RemoveStudent(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task GivenRemoveStudentFromGroupCommand_ReturnsSuccessStatusCode()
        {
            var client = _factory.CreateClient();

            var response = await client.DeleteAsync($"/api/groups/student?groupId=1&studentId=2");

            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GivenRemoveStudentFromGroupCommandWithInvalidStudentId_ReturnsSuccessStatusCode()
        {
            var client = _factory.CreateClient();

            var response = await client.DeleteAsync($"/api/groups/student?groupId=1&studentId=0");

            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GivenAddStudentToGroupCommandWithInvalidGroupId_ReturnsSuccessStatusCode()
        {
            var client = _factory.CreateClient();

            var response = await client.DeleteAsync($"/api/groups/student?groupId=0&studentId=1");

            response.EnsureSuccessStatusCode();
        }
        
    }
}
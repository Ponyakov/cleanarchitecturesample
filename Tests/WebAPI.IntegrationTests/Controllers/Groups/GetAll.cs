using System.Threading.Tasks;
using Application.Groups.Queries.GetGroupsList;
using WebAPI.IntegrationTests.Common;
using Xunit;

namespace WebAPI.IntegrationTests.Controllers.Groups
{
    public class GetAll : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> _factory;

        public GetAll(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task ReturnsGroupsListViewModel()
        {
            var client = _factory.CreateClient();

            var response = await client.GetAsync("/api/groups");

            response.EnsureSuccessStatusCode();

            var vm = await Utilities.GetResponseContent<GroupsListVm>(response);

            Assert.IsType<GroupsListVm>(vm);
            Assert.NotEmpty(vm.Groups);
        }

        
        [Fact]
        public async Task ReturnsNotEmptyGroupsListViewModelWithFilter()
        {
            var client = _factory.CreateClient();

            var response = await client.GetAsync("/api/groups?name=best");

            response.EnsureSuccessStatusCode();

            var vm = await Utilities.GetResponseContent<GroupsListVm>(response);

            Assert.NotEmpty(vm.Groups);
        }

        [Fact]
        public async Task ReturnsEmptyGroupsListViewModelWithFilter()
        {
            var client = _factory.CreateClient();

            var response = await client.GetAsync("/api/groups?name=qwerty");

            response.EnsureSuccessStatusCode();

            var vm = await Utilities.GetResponseContent<GroupsListVm>(response);

            Assert.Empty(vm.Groups);
        }

    }
}
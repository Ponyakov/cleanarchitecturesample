using System.Net;
using System.Threading.Tasks;
using Application.Groups.Commands.UpdateGroup;
using Application.Students.Commands.UpdateSudent;
using WebAPI.IntegrationTests.Common;
using Xunit;

namespace WebAPI.IntegrationTests.Controllers.Groups
{
    public class Update : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> _factory;

        public Update(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }
        
        [Fact]
        public async Task GivenUpdateStudentCommandWithInvalidId_ReturnsNotFoundStatusCode()
        {
            var client = _factory.CreateClient();

            var command = new UpdateGroupCommand
            {
                Id = 0,
                Name = "Group 1",
            };

            var content = Utilities.GetRequestContent(command);

            var response = await client.PutAsync($"/api/groups/{command.Id}", content);

            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GivenUpdateStudentCommand_ReturnsSuccessStatusCode()
        {
            var client = _factory.CreateClient();

            var command = new UpdateGroupCommand
            {
                Id = 1,
                Name = "Group 1"
            };

            var content = Utilities.GetRequestContent(command);

            var response = await client.PutAsync($"/api/groups/{command.Id}", content);

            response.EnsureSuccessStatusCode();
        }
    }
}
using System.Threading.Tasks;
using Application.Students.Queries.GetStudentsList;
using WebAPI.IntegrationTests.Common;
using Xunit;

namespace WebAPI.IntegrationTests.Controllers.Students
{
    public class GetAll : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> _factory;

        public GetAll(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task ReturnsStudentsListViewModel()
        {
            var client = _factory.CreateClient();

            var response = await client.GetAsync("/api/students");

            response.EnsureSuccessStatusCode();

            var vm = await Utilities.GetResponseContent<StudentsListVm>(response);

            Assert.IsType<StudentsListVm>(vm);
            Assert.True(vm.Students.Count == 2);
        }

        [Fact]
        public async Task ReturnsNotEmptyStudentsListViewModelWithGroupFilter()
        {
            var client = _factory.CreateClient();

            var response = await client.GetAsync("/api/students?group=best");

            response.EnsureSuccessStatusCode();

            var vm = await Utilities.GetResponseContent<StudentsListVm>(response);

            Assert.NotEmpty(vm.Students);
        }

        [Fact]
        public async Task ReturnsEmptyStudentsListViewModelWithGroupFilter()
        {
            var client = _factory.CreateClient();

            var response = await client.GetAsync("/api/students?group=qwerty");

            response.EnsureSuccessStatusCode();

            var vm = await Utilities.GetResponseContent<StudentsListVm>(response);

            Assert.Empty(vm.Students);
        }

        [Fact]
        public async Task ReturnsNotEmptyStudentsListViewModelWithFullNameFilter()
        {
            var client = _factory.CreateClient();

            var response = await client.GetAsync("/api/students?fullname=vlad");

            response.EnsureSuccessStatusCode();

            var vm = await Utilities.GetResponseContent<StudentsListVm>(response);

            Assert.NotEmpty(vm.Students);
        }

        [Fact]
        public async Task ReturnsEmptyStudentsListViewModelWithFullNameFilter()
        {
            var client = _factory.CreateClient();

            var response = await client.GetAsync("/api/students?fullname=qwerty");

            response.EnsureSuccessStatusCode();

            var vm = await Utilities.GetResponseContent<StudentsListVm>(response);

            Assert.Empty(vm.Students);
        }

        [Fact]
        public async Task ReturnsNotEmptyStudentsListViewModelWithNickNameFilter()
        {
            var client = _factory.CreateClient();

            var response = await client.GetAsync("/api/students?nickname=cucumber");

            response.EnsureSuccessStatusCode();

            var vm = await Utilities.GetResponseContent<StudentsListVm>(response);

            Assert.NotEmpty(vm.Students);
        }

        [Fact]
        public async Task ReturnsEmptyStudentsListViewModelWithNickNameFilter()
        {
            var client = _factory.CreateClient();

            var response = await client.GetAsync("/api/students?nickname=qwerty");

            response.EnsureSuccessStatusCode();

            var vm = await Utilities.GetResponseContent<StudentsListVm>(response);

            Assert.Empty(vm.Students);
        }

        [Fact]
        public async Task ReturnsNotEmptyStudentsListViewModelWithGenderFilter()
        {
            var client = _factory.CreateClient();

            var response = await client.GetAsync("/api/students?gender=male");

            response.EnsureSuccessStatusCode();

            var vm = await Utilities.GetResponseContent<StudentsListVm>(response);

            Assert.NotEmpty(vm.Students);
        }

        [Fact]
        public async Task ReturnsEmptyStudentsListViewModelWithGenderFilter()
        {
            var client = _factory.CreateClient();

            var response = await client.GetAsync("/api/students?gender=female");

            response.EnsureSuccessStatusCode();

            var vm = await Utilities.GetResponseContent<StudentsListVm>(response);

            Assert.Empty(vm.Students);
        }

        [Fact]
        public async Task ReturnsNotEmptyStudentsListViewModelWithPagination()
        {
            var client = _factory.CreateClient();

            var response = await client.GetAsync("/api/students?pagesize=1&pageindex=2");

            response.EnsureSuccessStatusCode();

            var vm = await Utilities.GetResponseContent<StudentsListVm>(response);

            Assert.NotEmpty(vm.Students);
        }

    }
}
using System.Threading.Tasks;
using Application.Students.Commands;
using Application.Students.Commands.CreateStudent;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc.Testing;
using WebAPI.IntegrationTests.Common;
using Xunit;

namespace WebAPI.IntegrationTests.Controllers.Students
{
    public class Create : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> _factory;

        public Create(WebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task GivenCreateStudentCommand_ReturnsSuccessStatusCode()
        {
            var client = _factory.CreateClient();

            var command = new CreateStudentCommand
            {
                FirstName = "Peter",
                Gender = 0,
                LastName = "Sidorov",
                NickName = "pomodoro",
                PatronymicName = "Yaroslavovich"
            };

            var content = Utilities.GetRequestContent(command);

            var response = await client.PostAsync($"/api/students", content);

            var id = await Utilities.GetResponseContent<int>(response);

            response.EnsureSuccessStatusCode();
        }
    }
}
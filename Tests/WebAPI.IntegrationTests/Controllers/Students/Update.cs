using System.Net;
using System.Threading.Tasks;
using Application.Students.Commands.UpdateSudent;
using WebAPI.IntegrationTests.Common;
using Xunit;

namespace WebAPI.IntegrationTests.Controllers.Students
{
    public class Update : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> _factory;

        public Update(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        
        [Fact]
        public async Task GivenUpdateStudentCommandWithInvalidId_ReturnsNotFoundStatusCode()
        {
            var client = _factory.CreateClient();

            var command = new UpdateStudentCommand
            {
                Id = 0,
                FirstName = "Peter",
                Gender = 0,
                LastName = "Sidorov",
                NickName = "pomodoro",
                PatronymicName = "Yaroslavovich"
            };

            var content = Utilities.GetRequestContent(command);

            var response = await client.PutAsync($"/api/students/{command.Id}", content);

            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GivenUpdateStudentCommand_ReturnsSuccessStatusCode()
        {
            var client = _factory.CreateClient();

            var command = new UpdateStudentCommand
            {
                Id = 1,
                FirstName = "Peter",
                Gender = 0,
                LastName = "Sidorov",
                NickName = "pomodoro",
                PatronymicName = "Yaroslavovich"
            };

            var content = Utilities.GetRequestContent(command);

            var response = await client.PutAsync($"/api/students/{command.Id}", content);

            var id = await Utilities.GetResponseContent<int>(response);

            response.EnsureSuccessStatusCode();
        }
    }
}
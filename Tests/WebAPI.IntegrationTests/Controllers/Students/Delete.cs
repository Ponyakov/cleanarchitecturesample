using System.Net;
using System.Threading.Tasks;
using WebAPI.IntegrationTests.Common;
using Xunit;

namespace WebAPI.IntegrationTests.Controllers.Students
{
    public class Delete : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> _factory;

        public Delete(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task GivenId_ReturnsSuccessStatusCode()
        {
            var client = _factory.CreateClient();

            var validId = "1";

            var response = await client.DeleteAsync($"/api/students/{validId}");

            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GivenInvalidId_ReturnsNotFoundStatusCode()
        {
            var client = _factory.CreateClient();

            var invalidId = "0";

            var response = await client.DeleteAsync($"/api/students/{invalidId}");

            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GivenInvalidId_ReturnsBadRequestStatusCode()
        {
            var client = _factory.CreateClient();

            var invalidId = "A";

            var response = await client.DeleteAsync($"/api/students/{invalidId}");

            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
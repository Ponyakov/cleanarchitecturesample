using System.Threading.Tasks;
using Application.Groups.Commands.AddStudentToGroup;
using Application.Groups.Commands.CreateGroup;
using Application.Groups.Commands.DeleteGroup;
using Application.Groups.Commands.RemoveStudentFromGroup;
using Application.Groups.Commands.UpdateGroup;
using Application.Groups.Queries.GetGroupsList;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    public class GroupsController : BaseController
    {
        [HttpGet]
        public async Task<ActionResult<GroupsListVm>> GetAll([FromQuery]GetGroupsListQuery query)
        {
            var vm = await Mediator.Send(query);

            return Ok(vm);
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<int>> Create(CreateGroupCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Update(UpdateGroupCommand command)
        {
            await Mediator.Send(command);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteGroupCommand { Id = id });

            return NoContent();
        }

        [HttpPost("student")]
        public async Task<ActionResult> AddStudent(AddStudentToGroupCommand command)
        {
            await Mediator.Send(command);

            return NoContent();
        }

        [HttpDelete("student")]
        public async Task<ActionResult> RemoveStudent([FromQuery]RemoveStudentFromGroupCommand command)
        {
            await Mediator.Send(command);

            return NoContent();
        }
    }
}
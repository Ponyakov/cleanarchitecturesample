using System.Threading.Tasks;
using Application.Groups.Commands.AddStudentToGroup;
using Application.Students.Commands.CreateStudent;
using Application.Students.Commands.DeleteStudent;
using Application.Students.Commands.UpdateSudent;
using Application.Students.Queries.GetStudentsList;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    public class StudentsController : BaseController
    {
        [HttpGet]
        public async Task<ActionResult<StudentsListVm>> GetAll([FromQuery]GetStudentsListQuery query)
        {
            var vm = await Mediator.Send(query);

            return Ok(vm);
        }

        [HttpPost]
        public async Task<ActionResult<int>> Create(CreateStudentCommand command)
        {
            return await Mediator.Send(command);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Update(UpdateStudentCommand command)
        {
            await Mediator.Send(command);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteStudentCommand { Id = id });

            return NoContent();
        }        
    }
}
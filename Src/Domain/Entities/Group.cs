using System.Collections.Generic;

namespace Domain.Entities
{
    public class Group
    {
        public Group()
        {
            GroupStudents = new HashSet<GroupStudent>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<GroupStudent> GroupStudents { get; private set; }
    }
}
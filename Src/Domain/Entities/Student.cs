using System.Collections.Generic;

namespace Domain.Entities
{
    public class Student
    {
        public Student()
        {
            GroupStudents = new HashSet<GroupStudent>();
        }
        public int Id { get; set; }
        public Gender Gender { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PatronymicName { get; set; }
        public string NickName { get; set; }
        public virtual ICollection<GroupStudent> GroupStudents { get; private set; }
    }
}
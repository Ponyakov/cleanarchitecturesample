using System.Threading;
using System.Threading.Tasks;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Application.Common.Interfaces
{
    public interface IApplicationDbContext
    {
        DbSet<Group> Groups { get; set; }

        DbSet<Student> Students { get; set; }

        DbSet<GroupStudent> GroupStudents { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.Groups.Queries.GetGroupsList
{
    public class GetGroupsListQuery : IRequest<GroupsListVm>
    {
        private string _name;

        public string Name { get => _name; set => _name = value.ToLower(); }
    }

    public class GetGroupsListQueryHandler : IRequestHandler<GetGroupsListQuery, GroupsListVm>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetGroupsListQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<GroupsListVm> Handle(GetGroupsListQuery request, CancellationToken cancellationToken)
        {
            var groups = await _context.Groups
                .Where(g => g.Name.ToLower().Contains(request.Name??string.Empty))
                .OrderBy(g => g.Name)
                .ProjectTo<GroupLookupDto>(_mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);

            var vm = new GroupsListVm
            {
                Groups = groups
            };

            return vm;
        }
    }
}
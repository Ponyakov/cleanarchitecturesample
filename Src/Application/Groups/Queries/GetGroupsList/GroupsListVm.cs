using System.Collections.Generic;

namespace Application.Groups.Queries.GetGroupsList
{
    public class GroupsListVm
    {
        public IReadOnlyList<GroupLookupDto> Groups { get; set; }
    }
}
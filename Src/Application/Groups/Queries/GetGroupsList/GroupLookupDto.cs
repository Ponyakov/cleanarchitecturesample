using System.Linq;
using Application.Common.Mappings;
using AutoMapper;
using Domain.Entities;


namespace Application.Groups.Queries.GetGroupsList
{
    public class GroupLookupDto : IMapFrom<Group>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Group, GroupLookupDto>()
                .ForMember(d => d.Count, opt => opt.MapFrom(s => s.GroupStudents.Count()));
        }
    }
}
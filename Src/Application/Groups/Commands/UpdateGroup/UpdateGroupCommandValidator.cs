using FluentValidation;

namespace Application.Groups.Commands.UpdateGroup
{
    public class UpdateGroupCommandValidator : AbstractValidator<UpdateGroupCommand>
    {
        public UpdateGroupCommandValidator()
        {
            RuleFor(v => v.Name)
                .MaximumLength(25)
                .NotEmpty();
        }
    }
}
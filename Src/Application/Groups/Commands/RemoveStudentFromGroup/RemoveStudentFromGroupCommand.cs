using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using MediatR;

namespace Application.Groups.Commands.RemoveStudentFromGroup
{
    public class RemoveStudentFromGroupCommand : IRequest
    {
        public int StudentId { get; set; }
        public int GroupId { get; set; }
    }
    public class RemoveStudentFromGroupCommandHandler : IRequestHandler<RemoveStudentFromGroupCommand>
    {
        private readonly IApplicationDbContext _context;

        public RemoveStudentFromGroupCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(RemoveStudentFromGroupCommand request, CancellationToken cancellationToken)
        {
            var links = _context.GroupStudents.Where(gs => gs.GroupId == request.GroupId 
                && gs.StudentId == request.StudentId);

            if (links.Count() == 0)
                return Unit.Value;

            _context.GroupStudents.RemoveRange(links);

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
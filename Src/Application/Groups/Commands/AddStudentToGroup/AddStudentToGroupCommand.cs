using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Domain.Entities;
using MediatR;

namespace Application.Groups.Commands.AddStudentToGroup
{
    public class AddStudentToGroupCommand : IRequest
    {
        public int StudentId { get; set; }
        public int GroupId { get; set; }
    }

    public class AddStudentToGroupCommandHandler : IRequestHandler<AddStudentToGroupCommand>
    {
        private readonly IApplicationDbContext _context;

        public AddStudentToGroupCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(AddStudentToGroupCommand request, CancellationToken cancellationToken)
        {
            var group = await _context.Groups.FindAsync(request.GroupId);

            if (group == null)
            {
                throw new NotFoundException(nameof(Group), request.GroupId);
            }

            var student = await _context.Students.FindAsync(request.StudentId);

            if (student == null)
            {
                throw new NotFoundException(nameof(Student), request.StudentId);
            }

            if (_context.GroupStudents.Any(gs => gs.GroupId == request.GroupId && gs.StudentId == request.StudentId))
            {
                return Unit.Value;
            }

            var entity = new GroupStudent
            {
                StudentId = request.StudentId,
                GroupId = request.GroupId
            };

            _context.GroupStudents.Add(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
using FluentValidation;

namespace Application.Groups.Commands.CreateGroup
{
    public class CreateGroupCommandValidator : AbstractValidator<CreateGroupCommand>
    {
        public CreateGroupCommandValidator()
        {
            RuleFor(v => v.Name)
                .MaximumLength(25)
                .NotEmpty();       
        }
    }
}
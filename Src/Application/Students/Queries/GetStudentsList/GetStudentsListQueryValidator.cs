using FluentValidation;

namespace Application.Students.Queries.GetStudentsList
{
    public class GetStudentsListQueryValidator : AbstractValidator<GetStudentsListQuery>
    {
        public GetStudentsListQueryValidator()
        {
            RuleFor(v => v.PageIndex)
                .GreaterThan(0);

            RuleFor(v => v.PageSize)
                .GreaterThan(0);
        }
    }
}
using System.Collections.Generic;
using System.Linq;
using Application.Common.Mappings;
using AutoMapper;
using Domain.Entities;

namespace Application.Students.Queries.GetStudentsList
{
    public class StudentLookupDto : IMapFrom<Student>
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string NickName { get; set; }
         public IReadOnlyList<string> Groups { get; set; }
        
        public void Mapping(Profile profile)
        {
            profile.CreateMap<Student, StudentLookupDto>()
                .ForMember(d => d.FullName, opt => opt.MapFrom(s => $"{s.FirstName} {s.PatronymicName} {s.LastName}"))
                .ForMember(d => d.Groups, opt => opt.MapFrom(s => s.GroupStudents.Select(gs => gs.Group.Name)));
        }
    }
}
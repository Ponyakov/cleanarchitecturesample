using System.Collections.Generic;

namespace Application.Students.Queries.GetStudentsList
{
    public class StudentsListVm
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int Count { get; set; }
        public IReadOnlyList<StudentLookupDto> Students { get; set; }
    }
}
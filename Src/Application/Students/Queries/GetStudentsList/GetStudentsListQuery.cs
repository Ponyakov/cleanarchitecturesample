using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.Students.Queries.GetStudentsList
{
    public class GetStudentsListQuery : IRequest<StudentsListVm>
    {
        private const int MaxPageSize = 50;

        private int _pageSize = 6;

        public int PageIndex { get; set; } = 1;

        public int PageSize
        {
            get => _pageSize;
            set => _pageSize = (value > MaxPageSize) ? MaxPageSize : value;
        }

        private string _fullName;
        private string _group;
        private string _nickName;

        public string Gender { get; set; }
        public string FullName { get => _fullName; set => _fullName = value.ToLower(); }
        public string Group { get => _group; set => _group = value.ToLower(); }
        public string NickName { get => _nickName; set => _nickName = value.ToLower(); }
    }

    public class GetStudentsListQueryHandler : IRequestHandler<GetStudentsListQuery, StudentsListVm>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetStudentsListQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<StudentsListVm> Handle(GetStudentsListQuery request, CancellationToken cancellationToken)
        {
            Gender gender;
            var hasGender = TryGetGender(request.Gender, out gender);

            Expression<Func<Student, bool>> expression = (s => (1 == (hasGender ? 0 : 1) || s.Gender == gender)
                    && (s.FirstName + " " + s.PatronymicName + " " + s.LastName).ToLower().Contains(request.FullName??string.Empty)
                    && s.NickName.ToLower().Contains(request.NickName??string.Empty)
                    && (1 == (string.IsNullOrEmpty(request.Group) ? 1 : 0) 
                        || s.GroupStudents.Any(gs => gs.Group.Name.ToLower().Contains(request.Group??string.Empty))));

            var students = await _context.Students
                .Where(expression)
                .OrderBy(s => s.FirstName + " " + s.PatronymicName + " " + s.LastName)
                .Skip((request.PageIndex - 1) * request.PageSize)
                .Take(request.PageSize)
                .ProjectTo<StudentLookupDto>(_mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);

            var totalCount = await _context.Students
                .Where(expression).CountAsync();

            var vm = new StudentsListVm
            {
                PageIndex = request.PageIndex,
                PageSize = request.PageSize,
                Count = totalCount,
                Students = students
            };

            return vm;
        }

        private bool TryGetGender(string filter, out Gender gender)
        {
            gender = Gender.Male;

            if (filter == null)
                return false;

            if ("male".StartsWith(filter, StringComparison.InvariantCultureIgnoreCase))
            {
                return true;
            }

            if ("female".StartsWith(filter, StringComparison.InvariantCultureIgnoreCase))
            {
                gender = Gender.Female;
                return true;
            }

            return false;
        }
    }
}
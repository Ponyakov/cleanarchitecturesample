using System.Linq;
using Application.Common.Interfaces;
using FluentValidation;

namespace Application.Students.Commands.UpdateSudent
{
public class UpdateStudentCommandValidator : AbstractValidator<UpdateStudentCommand>
    {
        public UpdateStudentCommandValidator(IApplicationDbContext context)
        {
            RuleFor(v => v.Gender)
                .InclusiveBetween(0,1);

            RuleFor(v => v.FirstName)
                .MaximumLength(40)
                .NotEmpty();

            RuleFor(v => v.LastName)
                .MaximumLength(40)
                .NotEmpty();

            RuleFor(v => v.PatronymicName)
                .MaximumLength(60);

            RuleFor(v => v.NickName)
                .MinimumLength(6)
                .MaximumLength(16)
                .Must(name => !IsDuplicate(name, context));
        }

        private bool IsDuplicate(string name, IApplicationDbContext context)
        {
            return context.Students.Any(s => s.NickName == name);
        }
    }
}
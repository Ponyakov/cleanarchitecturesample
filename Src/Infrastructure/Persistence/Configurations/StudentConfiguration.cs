using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configurations
{
    public class StudentConfiguration : IEntityTypeConfiguration<Student>
    {
        public void Configure(EntityTypeBuilder<Student> builder)
        {
            builder.Property(t => t.FirstName)
                .HasMaxLength(40)
                .IsRequired();

            builder.Property(t => t.LastName)
                .HasMaxLength(40)
                .IsRequired();

            builder.Property(t => t.PatronymicName)
                .HasMaxLength(60);

            builder.Property(t => t.NickName)
                .HasMaxLength(16);

            builder.HasIndex(s => s.NickName)
                .IsUnique();
        }
    }
}
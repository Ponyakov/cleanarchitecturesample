using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configurations
{
    public class GroupStudentConfiguration : IEntityTypeConfiguration<GroupStudent>
    {
        public void Configure(EntityTypeBuilder<GroupStudent> builder)
        {
            builder.HasKey(k => new { k.GroupId, k.StudentId } );

            builder.HasOne(gs => gs.Group)
                .WithMany(g => g.GroupStudents)
                .HasForeignKey(k => k.GroupId);

            builder.HasOne(gs => gs.Student)
                .WithMany(s => s.GroupStudents)
                .HasForeignKey(k => k.StudentId);
        }
    }
}